# Iridium
A register-based VM built in Rust following this [subnetzero.io tutorial](https://blog.subnetzero.io/post/building-language-vm-part-00/). Our goals are:

1. Erlang VM-level resiliency
2. Tight integration with a language (see the Palladium project)
3. Horizontal scalability



## Manual
You can find the details of how the VM works here: https://gitlab.com/subnetzero/iridium/blob/master/docs/manual.adoc

## Roadmap
You can find the roadmap here: https://gitlab.com/subnetzero/iridium/blob/master/docs/roadmap.adoc
